/**
 * @file XXX_test.cc
 * @brief NPDE homework XXX code
 * @author
 * @date
 * @copyright Developed at SAM, ETH Zurich
 */

#include <gtest/gtest.h>

#include <Eigen/Core>

#include "../XXX.h"

namespace XXX::test {}  // namespace XXX::test
