/**
 * @file coupledbvps.cc
 * @brief NPDE homework CoupledBVPs code
 * @author R. Hiptmair
 * @date June 2023
 * @copyright Developed at SAM, ETH Zurich
 */

#include <Eigen/Core>

#include "coupledbvps.h"

namespace CoupledBVPs {}  // namespace CoupledBVPs
